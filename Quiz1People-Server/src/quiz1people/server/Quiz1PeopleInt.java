/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1people.server;

import java.io.IOException;

/**
 *
 * @author ipd
 */
public interface Quiz1PeopleInt {
    public int addPeople(String people) throws IOException;
    public String getAllPeople() throws IOException;
}

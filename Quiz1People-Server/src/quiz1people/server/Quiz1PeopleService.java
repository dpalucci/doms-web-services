/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1people.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import static javax.swing.Spring.height;

/**
 *
 * @author ipd
 */
public class Quiz1PeopleService implements Quiz1PeopleInt {
    
    private static final String FILE_NAME = "data.txt";
        

    @Override
    public String getAllPeople() throws IOException {
        System.out.println("called getAllPeople()");
        try (Scanner scanner = new Scanner(new File(FILE_NAME))) {
            ArrayList<String> peoppleList = new ArrayList<>();
            // trick scanner into returning content of the entire text file
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.toLowerCase().contains(keyword.toLowerCase())) {
                    peopleList.add(line);
                }
            }
            // scanner.close();
            return peopleList;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
            //return "";
        }
    }

    @Override
    public int addPeople(String people) throws IOException {
        public int addPeople(String name, int age, int height) throws IOException {
        System.out.println("called addPeople()");
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME, true)))) {
            String people = String.format("%s;%d;%d", name, age, height);
            pw.println(people);
            //pw.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return 0;       
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remotelib.server;

import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;


public class RemoteLibServer {
    private static final int port = 8282;
    
    public static void main(String[] args) throws Exception {
        WebServer webServer = new WebServer(port);
        XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();

        //tell XML-RPC sever which classes contain methods to map
        PropertyHandlerMapping phm = new PropertyHandlerMapping();     
        phm.addHandler("RemoteLib", remotelib.server.RemotelibService.class);  
        xmlRpcServer.setHandlerMapping(phm);
        
           //set some additional options
        XmlRpcServerConfigImpl serverConfig
                = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
        serverConfig.setEnabledForExtensions(true);
        serverConfig.setContentLengthOptional(false);
        
        //start webserver and don't stop
        webServer.start();
    }
    
}

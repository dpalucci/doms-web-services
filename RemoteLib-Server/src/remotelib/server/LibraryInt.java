/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remotelib.server;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ipd
 */
public interface LibraryInt {
    public int addBook(String title, String author, int yop);
    public ArrayList<String>getAllBooks() throws IOException;
    
}

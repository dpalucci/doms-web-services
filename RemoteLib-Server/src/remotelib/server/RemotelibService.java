/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remotelib.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class RemotelibService implements LibraryInt {
    private static final String FILE_NAME = "data.txt";

    @Override
    public int addBook(String title, String author, int yop) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("called addBook()");
        try {
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME, true)));
            String book = String.format("%1;%2;%3", title; author; yop);
            pw.println(book);
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return 0;
    }

    @Override
    public ArrayList<String>getAllBooks() throws IOException {
        System.out.println("called getAllBooks()");
        try {
            ArrayList<String> bookList = new ArrayList<>();
            // trick scanner into returning content of the entire text file
            Scanner scanner = new Scanner(new File(FILE_NAME)).useDelimiter("\u001A");
            return scanner.next();
        } 
       // scanner.close();
        return bookList;
    }
        catch (IOException e) {
            e.printStackTrace();
            throw e;
            //return "";
        }
    }
    
}

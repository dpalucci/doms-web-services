/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1people.client;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.xmlrpc.XmlRpcException;

/**
 *
 * @author ipd
 */
public class Quiz1PeopleProxy implements Quiz1PeopleInt {

    @Override
    public int addPeople(String name, int age, int height) throws IOException {
        try {
           Object[] params = new Object[]{ name, age, height };
           return (Integer) rpc.execute("Remotelibrary.addPeople", params);
        } catch (XmlRpcException ex) {
            throw new IOException(ex);           
            // alternative: extract the original exception using
            // throw ex.getCause();
        }
    }

    @Override
    public ArrayList<String> getFilteredPeople(String keyword) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

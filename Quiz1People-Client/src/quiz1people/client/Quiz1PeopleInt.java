/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1people.client;

import java.io.IOException;
import java.util.ArrayList;

public interface Quiz1PeopleInt {
    public int addPeople(String name, int age, int height) throws IOException;
    public ArrayList<String> getFilteredPeople(String keyword) throws IOException; 
}

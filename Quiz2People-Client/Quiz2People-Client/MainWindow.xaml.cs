﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2People_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static PeopleReference.PeopleSoapClient proxy;
        String filter = "";
        List<string> peopleList;
        public MainWindow()
        {
            proxy = new PeopleReference.PeopleSoapClient();
            InitializeComponent();
            peopleList = new List<string>();
            lvList.ItemsSource = peopleList;
            RefreshListView();
        }

        private void tbAdd_Click(object sender, RoutedEventArgs e)
        {
            String name = tbName.Text;
            int age;
            Int32.TryParse(tbAge.Text, out age);
            int height;
            Int32.TryParse(tbHeight.Text, out height);
            //validate
            proxy.AddPerson(name, age, height);
            filter = "";
            tbFilter.Text = "";
            RefreshListView();

        }

        private void RefreshListView()
        {
            peopleList = proxy.GetFilteredPeople(filter);
            lvList.ItemsSource = peopleList;
        }
        private void btFilter_Click(object sender, RoutedEventArgs e)
        {
            filter = tbFilter.Text;
            RefreshListView();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Quiz2People_Server
{
    /// <summary>
    /// Summary description for People
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class People : System.Web.Services.WebService
    {
        private static readonly String FILE_NAME = @"C:\Users\ipd\Documents\Dom_WebServices\Quiz2People-Server\data.txt";

        [WebMethod]
        public void AddPerson(String name, int age, int height)
        {
            String person = string.Format("{0}; {1}; {2}", name, age, height);
            try
            {
                File.AppendAllText(FILE_NAME, person + Environment.NewLine);
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
                throw e;
            }
        }

        [WebMethod]
        public List<string> GetFilteredPeople(string keyword)
        {

            try
            {
                String[] lines = File.ReadAllLines(FILE_NAME);
                List<string> peoplelist = new List<string>();
                foreach (var people in lines)
                {
                    if (people.ToLower().Contains(keyword.ToLower()))
                    {
                        peoplelist.Add(people);
                    }
                }
                return peoplelist;
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                // Console.ReadKey();
                throw e;
            }
        }
    }
}
